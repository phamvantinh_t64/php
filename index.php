
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    
<title>Day 07</title>
<style>
@import url("https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap");
</style>
</head>
<body>
<?php
  if($_SERVER["REQUEST_METHOD"] == "POST"){
    $_COOKIE["faculty"] = $_POST["faculty"];
    $_COOKIE["keyword"] = $_POST["keyword"];
  }
?>
<div class="container" style="height: auto; width: 750px; margin: 15px auto; border: 1px solid #5b9bd5; justify-content:center; flex-direction: column;">
 <!-- align-items: center; -->
<div style="width: 450px; text-align: left;">
<label style="color: red">

</label>
</div>
<form method="post"  action="">


 <!-- Select faculty -->
<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white"> Phân khoa</label>
</div>
<select name="faculty" id="faculty" style="margin-left: 20px; height: 40px; width: 170px; border: 1px solid #42719b;">
<?php
$faculties = array(array("", ""), array("MAT", "Khoa học máy tính"), array("KDL", "Khoa học vật liệu"));

foreach ($faculties as $faculty) {
  ?>
    <option <?php if(isset($_COOKIE["faculty"])){
      if($_COOKIE["faculty"] == $faculty[0]){
        echo "selected";
      }
     } ?> value=<?php echo $faculty[0]; ?>><?php echo $faculty[1]; ?></option>
    <?php
}
?>
</select>

</div>
<!-- address -->

<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white" > Từ khóa </label>
</div>
<input value="<?php if(isset($_COOKIE["keyword"])){ echo $_COOKIE["keyword"];} ?>" type="text" id="keyword" name="keyword" style="margin-left: 20px; height: 40px; width: 170px; border: 1px solid #42719b;">
</div>


<div style="width: 300px; margin: 0 auto; display: flex; justify-content: center;">
<button type="" class="btn btn_clear" id="btn_clear" onlick style="margin: 0 10px; height: 45px; width: 130px; font-size: 15px; background-color: #5b9bd5; border-radius: 5px; color: white;">Xóa</button>
<input type="submit" value="Tìm kiếm" style="height: 45px; width: 130px; font-size: 15px; background-color: #5b9bd5; border-radius: 5px; color: white;">
</div>

</form>
<div class="row" style = "margin: 10px 0 0 5px;">
    <label style="color: black" >Số sinh viên tìm thấy:</label> <span>XXX</span>
</div>
<div style="width: 300px;margin: 0 0px 0 500px; display: flex; justify-content: center;">
<form action="regist.php">
<button type="submit" value="Tìm kiếm" style="height: 30px; width: 70px; font-size: 15px; background-color: #5b9bd5; border-radius: 3px; color: white;">Thêm</button>
</form>
</div>
<div class="table-content">
<table class="table">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Tên sinh viên</th>
      <th scope="col">Khoa</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php
$students = array(
        "Trần Văn A"=>"Khoa học máy tính",
        "Nguyễn Thị B"=>"Khoa học máy tính",
        "Hoàng Văn C"=>"Khoa học dữ liệu",
        "Đinh Thị D"=>"Khoa học dữ liệu",
);
$stt =0;
foreach ($students as $name => $fac) {
   $stt++;
?>
  <tr>
      <th scope="row"><?php echo $stt; ?></th>
      <td><?php echo $name; ?></td>
      <td style="width:40%"><?php echo $fac; ?></td>
      <td style="width:20%">
        <button style="background-color: #5b9bd5; border-radius: 3px; color: white;">Xóa</button>
        <button style="background-color: #5b9bd5; border-radius: 3px; color: white;">Sửa</button>

    </td>
    </tr>
    <?php 
    }
?>
    
    
  </tbody>
</table>
</div>
</div>


</body>
</html>
<script type="text/javascript">

        $(document).ready(function() {

          $(document).on('click', '.btn_clear', function(e) {
              e.preventDefault();
              document.getElementById('keyword').value = '';
              document.querySelector('#faculty').value = '';
          });

          });
    </script>